<?php

namespace App\Twig;

use App\Repository\CategoryRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    /*public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }*/

    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /*
        getFunctions : creer une fonction personalisé dans Twig
    */
    public function getFunctions(): array
    {
        /*
            parametres:
                nom de la fonction das twig
                nom de la methode php reliée à la fonction
        */
        return [
            new TwigFunction('get_categories', [$this, 'getCatgories']),
        ];
    }

    public function getCatgories():array
    {
        /*
            doctrine : 
                repository : essentiellement à faire des SELECT
                EntityManager : UPDATE, INSERT et DELETE

            Méthode de sélection des repository
                find : récuperation d'une entité par son id
                findAll : array d'entités
                findBy : array d'entités avec conditions
                findOneBy : une entités avec conditions
        */
        $results = $this->categoryRepository->findAll();
        //dd($results);
        return $results;
    }
}