<?php

namespace App\Controller;

use App\Form\SearchGifType;
use App\Repository\SearchRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{
	private Request $request;
	private SearchRepository $searching;
	/**
	 * @Route("/index", name="search.index")
	*/
	public function research(SearchRepository $searching, Request $request):Response
	{
		$type = SearchGifType::class; 

		$searchForm = $this->createForm($type);
		$searchForm->handleRequest($request);

		//$donnees = $searching->findAll();
		
		if ($searchForm->isSubmitted()&& $searchForm->isValid()) {
            // get data from research
			$recherche = $searchForm->getData();

			$donnees = $searching->search($recherche);

			if ($donnees == null) {

				$this->addFlash('erreur', 'Aucun gif ne contient ce mot clé.');
				
            }
			
            //return $this->redirectToRoute('search/recherche.html.twig');
		}


		return $this->render('search/recherche.html.twig',[ 
			'searchForm' => $searchForm->createView() 
		]); 
	}
}

