<?php

namespace App\Repository;

use Doctrine\ORM\Query;
use App\Entity\Category;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;


class SearchRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        //
    }

    public function search($recherche)
    {
        $this->createQueryBuilder('c')
            ->andWhere('c.name = :recherche')
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

 
}
